from django.urls import path,include

from .views import BlogListView, BlogDetailView, BlogCreateView, BlogUpdateView, BlogDeleteView

urlpatterns = [
    path('', BlogListView.as_view(), name='home'),
    path('user/<int:pk>/', BlogDetailView.as_view(), name='user_detail'),
    path('user/new/', BlogCreateView.as_view(), name='user_new'),
    path('user/<int:pk>/edit/', BlogUpdateView.as_view(), name='user_edit'),
    path('user/<int:pk>/delete/', BlogDeleteView.as_view(), name='user_delete'),
]