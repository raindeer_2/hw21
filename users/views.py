from django.shortcuts import render

from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.urls import reverse_lazy


from .models import Users


class BlogListView(ListView):
    model = Users
    template_name = 'home.html'



class BlogDetailView(DetailView):
    model = Users
    template_name = 'user_detail.html'
    fields='__all__'

class BlogCreateView(CreateView):
    model = Users
    template_name = 'user_new.html'
    fields = '__all__'


class BlogUpdateView(UpdateView):
    model = Users
    template_name = 'user_edit.html'
    fields = '__all__'


class BlogDeleteView(DeleteView):
    model = Users
    template_name = 'user_delete.html'
    success_url = reverse_lazy('home')








